# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kirigami2
pkgver=5.54.0
pkgrel=0
pkgdesc="A QtQuick based components set"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL"
depends="qt5-qtgraphicaleffects"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev qt5-qtquickcontrols2-dev
			kpackage-dev kcoreaddons-dev kservice-dev kconfig-dev kwindowsystem-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-lang"
options="!check"
builddir="$srcdir/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"
	cmake "$srcdir"/$pkgname-$pkgver \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DKDE_INSTALL_LIBDIR=lib \
		-DBUILD_EXAMPLES=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="c85dbc8790faed84e7b59f703966f4404fe6a7c7d7e83ba4ae6d6469cd01d2a594aa7b5fbda460839df504a23a38b1881748c9a21071cc68de050e3760f25406  kirigami2-5.54.0.tar.xz"
